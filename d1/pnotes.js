// Client-server architecture
/* 
    Develop a static front-end application
    Create and manipulate a MongoDB database

*/
/* 
    What if we need our front-end app to show dynamic content? We'd need to send requests to a server that would in turn query and manipulate the database for us.

*/
/* 
    CLIENT(s) => request => SERVER => response => CLIENT(s)

*/
/* 
    Benefits
        - Centralized data makes applications more scalable and maintainable
        - Multiple client apps may all use dynamically-generated data
        - Workload is concentrated on the server, making client apps lightweight
        - Improves data availability

*/
/* 
    For JS developers, Node.JS for building server-side application

*/
// What is Node.js
/* 
    An open-source, Javascript runtime environment for creating server-side applications

*/
// Runtime Environment
/* 
    Gives the context for running a programming language
    JS was initially within the context of a browser
    Node.JS, the context was taken out of the browser and put into the server
    Node.Js, javascript now has access to the following
        system resources
        memory
        file system
        network
        etc.

*/
/* 
    Benefits
        Optimized for web applications
    
    Familiarity
        "Same old" Javascript

    Access to node package manager(NPM)
        World's largest registry of packages

*/